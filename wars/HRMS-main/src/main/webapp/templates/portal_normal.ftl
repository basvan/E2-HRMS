<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
    <title>${the_title} - ${company_name}</title>

    <meta content="initial-scale=1.0, width=device-width" name="viewport" />

    <@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid" id="wrapper">
    <div class="head">
    <div class="col-md-2">
    <header id="banner" role="banner">
        <div id="heading">
            <h1 class="site-title">
                <a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
                    <img alt="${logo_description}" height="${site_logo_height}" src="${site_logo}" width="${site_logo_width}" />
                </a>

                <#if show_site_name>
                    <span class="site-name" title="<@liferay.language_format arguments="${site_name}"  key="go-to-x" />" style="margin-left: -11px;">
                        ${site_name}
                    </span>
                </#if>
            </h1>
      

       </div>
    </header>
    </div>
  
    <div class="col-md-8" id="nav_background" style="display: block;color: white;text-align: center;">
    <#if has_navigation && is_setup_complete>
            <#include "${full_templates_path}/navigation.ftl" />
     </#if>
      
    </div>
    <div class="col-md-1" id="signin" style="margin-top: 34px;">
    <#if !is_signed_in>
           <a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">${sign_in_text}</a>
        </#if>
    </div>
    
    
    </div>
    <section id="content">
        <h1 class="hide-accessible">${the_title}</h1>

        <#if selectable>
            <@liferay_util["include"] page=content_include />
        <#else>
            ${portletDisplay.recycle()}

            ${portletDisplay.setTitle(the_title)}

            <@liferay_theme["wrap-portlet"] page="portlet.ftl">
                <@liferay_util["include"] page=content_include />
            </@>
        </#if>
    </section>

    <footer id="footer" role="contentinfo">
      <#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
	<#assign theme_groupID = htmlUtil.escape(theme_display.getCompanyGroupId()?string) />
	<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", '${group_id}') />
	<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", "30894") />
      <@liferay_portlet["runtime"]
        defaultPreferences="${freeMarkerPortletPreferences}"
        portletProviderAction=portletProviderAction.VIEW
        instanceId="Footer"
        instanceId="30894"
        portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />
        ${freeMarkerPortletPreferences.reset()}

            </footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>